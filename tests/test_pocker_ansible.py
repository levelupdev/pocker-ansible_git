# encoding: utf-8
import sys
import os
import time
from copy import deepcopy
from collections import Counter
from pprint import pprint

import pytest

from pocker import Docker
from pocker_ansible.filter_plugins.pocker import pocker_env
from pocker_ansible.library.pocker_cmd import pocker_cmd
from pocker_ansible.library.pocker_img import pocker_img, _PockerFail as PImgFail
from pocker_ansible.library.pocker_cnt import pocker_cnt, _PockerFail as PCntFail
from pocker_ansible.library.pocker_net import pocker_net
from pocker_ansible.library.pocker_vol import pocker_vol


@pytest.yield_fixture(scope='session')
def docker(request):
    yield Docker()


@pytest.yield_fixture(scope='session')
def image(request, docker):
    #create test image(fixture) inside docker-in-docker
    context_file = "/home/vagrant/source/dev/test-img-context.tar.gz"
    docker.rmi('test-img', _suppress_excp=True)
    with open(context_file, mode='rb') as file: # b is important -> binary
        context = file.read()
    assert docker.build('-', tag='test-img', _input=context)

    yield

    assert docker.rmi('test-img')


@pytest.yield_fixture(scope='function')
def cmd_params(request):
    def get_params(**kwargs):
        params = {'cmd_args': None, 'cmd_opts': {}, 'docker_opts': {}}
        params.update(**kwargs)
        return params

    yield get_params


@pytest.yield_fixture(scope='function')
def img_params(request):
    def get_params(**kwargs):
        params = {'docker_opts': {}}
        params.update(kwargs)
        return params

    yield get_params


@pytest.yield_fixture(scope='function')
def cnt_params(request):
    def get_params(**kwargs):
        params = {'state': None, 'beacon': None, 'cnt_cmd': None, 'cnt_img': None,
                  'cnt_opts':{}, 'attach': False, 'kill_on_stop': False,
                  'count': 'all', 'docker_opts': {}, 'wait_for': {},
                  'order': {}, 'filters': {}, 'states': {},
                  'get_logs': False}
        params.update(kwargs)
        return params

    yield get_params


@pytest.yield_fixture(scope='function')
def net_params(request):
    def get_params(**kwargs):
        params = {'connected': [], 'disconnected': [], 'docker_opts': {},
                  'disconnect_others': False, 'net_opts': {}}
        params.update(**kwargs)
        return params

    yield get_params


@pytest.yield_fixture(scope='function')
def vol_params(request):
    def get_params(**kwargs):
        #TODO
        params = {'docker_opts': {}, 'vol_opts': {}}
        params.update(**kwargs)
        return params

    yield get_params


def test_filter_pocker_env():
    assert pocker_env({}) == []
    assert pocker_env({'GET_ENV_FROM_HOST': None}) == [u'GET_ENV_FROM_HOST']
    envs = pocker_env({'ONE': 'one', 'TWO': 'two'})
    assert  len(envs) == 2
    assert u'ONE=one' in envs
    assert u'TWO=two' in envs


def test_pocker_cmd_check_cmd_works(image, cmd_params):
    params = cmd_params
    p = params(cmd='version')
    assert pocker_cmd(p).result['Client']['Version'] is not None


def test_pocker_cmd_check_cmd_args_works(image, cmd_params):
    params = cmd_params
    p = params(cmd='inspect', cmd_args='test-img')
    assert pocker_cmd(p).result[0]['Os'] == 'linux'


def test_pocker_cmd_check_cmd_opts_works(image, cmd_params):
    params = cmd_params
    p = params(cmd='run',
               cmd_args='test-img sleep 10',
               cmd_opts={
                   'd': True,
                   'name': 'cmd_opts_test'
               })
    assert pocker_cmd(p).result
    p = params(cmd='rm',
               cmd_args='cmd_opts_test',
               cmd_opts={
                   'force': True,
               })
    pocker_cmd(p).result


def test_pocker_img_present_updated_absent(image, img_params):
    params = img_params
    p = params(state='present', image='tianon/true:latest')
    assert pocker_img(p)
    assert pocker_img(p).msg == "Image already present on host."

    p = params(state='updated', image='tianon/true:latest')
    assert pocker_img(p)

    p = params(state='absent', image='tianon/true:latest')
    assert pocker_img(p).changed == True
    assert pocker_img(p).changed == False


def test_pocker_img_published(image, img_params):
    params = img_params
    p = params(state='published', image='test-img:latest')
    try:
        pocker_img(p)
    except PImgFail as err:
        assert 'not implemented yet' in err.message


def test_pocker_img_unknown_state(image, img_params):
    params = img_params
    p = params(state='unknown', image='test-img:latest')
    try:
        pocker_img(p)
    except PImgFail as err:
        assert 'Unknown state' in err.message


def test_pocker_img_tag_for_image_must_be_specified(image, img_params):
    params = img_params
    p = params(state='present', image='test-img')
    try:
        pocker_img(p)
    except PImgFail as err:
        assert 'Tag for image must be specified' in err.message


def test_pocker_cnt_cant_change_state_of_absent_container(image, cnt_params, docker):
    params = cnt_params
    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='started', beacon='test_cnt', count=1 )
    try:
        pocker_cnt(p)
    except PCntFail as err:
        assert "To create container provide at least 'cnt_img'" in err.message


def test_pocker_cnt_present_started_paused_unpaused_restarted(image, cnt_params, docker):
    params = cnt_params
    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='present', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sleep 100', cnt_opts={'name': 'test_cnt_name'})
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is False

    p = params(state='started', beacon='test_cnt')
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True

    p = params(state='paused', beacon='test_cnt')
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Paused'] is True
    p = params(state='started', beacon='test_cnt')
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Paused'] is False

    p = params(state='restarted', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True

    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)


def test_pocker_cnt_restarting_state(image, cnt_params, docker):
    params = cnt_params
    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='started', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sh -c "exit 1"', cnt_opts={
                'name': 'test_cnt_name',
                'restart': 'always'
               })
    assert pocker_cnt(p)
    #wait till container restarts several times, so delay between start increased
    time.sleep(1)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    assert docker.inspect('test_cnt_name').result[0]['State']['Restarting'] is True
    restarts1 = docker.inspect('test_cnt_name').result[0]['RestartCount']

    p = params(state='started', beacon='test_cnt')
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    assert docker.inspect('test_cnt_name').result[0]['State']['Restarting'] is True
    restarts2 = docker.inspect('test_cnt_name').result[0]['RestartCount']
    #ensure that container was stopped and started again
    assert restarts2 < restarts1

    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)


    restarts1 = restarts2 = None

    p = params(state='started', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sh -c "exit 1"', cnt_opts={
                'name': 'test_cnt_name',
                'restart': 'always'
               })
    assert pocker_cnt(p)
    #wait till container restarts several times, so delay between start increased
    time.sleep(1)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    assert docker.inspect('test_cnt_name').result[0]['State']['Restarting'] is True
    restarts1 = docker.inspect('test_cnt_name').result[0]['RestartCount']

    p = params(state='started', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sh -c "exit 1"', cnt_opts={
                'name': 'test_cnt_name',
                'restart': 'no'
               })
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    restarts2 = docker.inspect('test_cnt_name').result[0]['RestartCount']
    #ensure that container was stopped and started again
    assert restarts2 < restarts1

    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)


def test_pocker_cnt_reloaded_force_reloaded(image, cnt_params, docker):
    params = cnt_params
    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='present', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sleep 100', cnt_opts={'name': 'test_cnt_name'})
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is False

    p = params(state='reloaded', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sleep 100', cnt_opts={'name': 'test_cnt_name'})
    assert pocker_cnt(p).changed is True
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    p = params(state='reloaded', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sleep 150', cnt_opts={'name': 'test_cnt_name'},
               kill_on_stop=True)
    assert pocker_cnt(p).changed is True
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    p = params(state='reloaded', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sleep 150', cnt_opts={'name': 'test_cnt_name'})
    assert pocker_cnt(p).changed is False
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    p = params(state='force_reloaded', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p).changed is True
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    p = params(state='force_reloaded', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p).changed is True
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True

    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)


def test_pocker_cnt_stopped_killed_absent(image, cnt_params, docker):
    params = cnt_params
    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='started', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sleep 100', cnt_opts={'name': 'test_cnt_name'})
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True

    p = params(state='stopped', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is False

    p = params(state='started', beacon='test_cnt')
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is True
    p = params(state='killed', beacon='test_cnt')
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is False

    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)
    assert 'test_cnt' not in [cnt['Name'] for cnt in docker.ps().result]

    p = params(state='started', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sleep 1', cnt_opts={'name': 'test_cnt_name'},
               attach=True)
    assert pocker_cnt(p)
    assert docker.inspect('test_cnt_name').result[0]['State']['Running'] is False
    docker.rm('test_cnt_name', force=True)


def test_pocker_cnt_unknown_state(image, cnt_params, docker):
    params = cnt_params
    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    try:
        p = params(state='unknown', beacon='test_cnt')
        pocker_cnt(p)
    except PCntFail as err:
        assert 'Unknown state' in err.message


def cnt_count(docker):
    ids = [cnt['Id'] for cnt in docker.ps(all=True).result]
    inspects = docker.inspect(*ids).result if ids else []
    states = []
    for cnt in inspects:
        if cnt['State']['Paused'] is True:
            states.append('paused')
        elif cnt['State']['Running'] is True:
            states.append('running')
        else:
            states.append('not_running')
    count = Counter(states)
    count['total'] = sum(count.values())
    return count


def test_pocker_cnt_multiple_containers_states_system(image, cnt_params, docker):
    params = cnt_params

    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(states=dict(present=6), beacon='test_cnt', cnt_img='test-img',
               cnt_cmd='sleep 100')
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 6
    assert count['not_running'] == 6

    p = params(states=dict(present=2, started=2, paused=1, leftover='absent'),
               beacon='test_cnt')
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 5
    assert count['not_running'] == 2
    assert count['running'] == 2
    assert count['paused'] == 1

    p = params(states=dict(started=3, leftover='stopped'),
               beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 5
    assert count['not_running'] == 2
    assert count['running'] == 3

    p = params(states=dict(started=3),
               beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p).changed == False
    count = cnt_count(docker)
    assert count['total'] == 5
    assert count['not_running'] == 2
    assert count['running'] == 3

    p = params(states=dict(force_reloaded=3),
               beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p).changed == True
    count = cnt_count(docker)
    assert count['total'] == 5
    assert count['not_running'] == 2
    assert count['running'] == 3

    p = params(state='absent', count='all',
               beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 0


def test_pocker_cnt_states_system_ordering(image, cnt_params, docker):
    params = cnt_params

    #ensure clean state
    p = params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(states=dict(present=2, started=2, paused=2), beacon='test_cnt',
               cnt_img='test-img', cnt_cmd='sleep 1000'
               )
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 6
    assert count['not_running'] == 2
    assert count['running'] == 2
    assert count['paused'] == 2

    p = params(states=dict(absent=2), beacon='test_cnt',
               order={'state': ['created_or_stopped']})
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 4
    assert count['running'] == 2
    assert count['paused'] == 2

    #to create new container is needed to provide same argument and options
    p = params(states=dict(present=2, started=2, paused=2), beacon='test_cnt',
               order={'state': ['deleted', 'running', 'paused']})
    assert pocker_cnt(p).changed == True
    count = cnt_count(docker)
    assert count['total'] == 6
    assert count['not_running'] == 2
    assert count['running'] == 2
    assert count['paused'] == 2

    p = params(states=dict(present=2, started=2, paused=2), beacon='test_cnt',
               order={'state': ['created_or_stopped', 'running', 'paused']})
    assert pocker_cnt(p).changed == False #ensure no containers changed
    count = cnt_count(docker)
    assert count['total'] == 6
    assert count['not_running'] == 2
    assert count['running'] == 2
    assert count['paused'] == 2

    #start 2 created containers and stop others
    p = params(states=dict(started=2, stopped=4), beacon='test_cnt',
               order={'state': ['created_or_stopped']},
               kill_on_stop=True)
    assert pocker_cnt(p).changed == True
    count = cnt_count(docker)
    assert count['total'] == 6
    assert count['not_running'] == 4
    assert count['running'] == 2


    p = params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 0


def test_pocker_cnt_states_system_filtering(image, cnt_params, docker):
    params = cnt_params

    #ensure clean state
    p = params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(states=dict(present=2, started=2, paused=2), beacon='test_cnt',
               cnt_img='test-img', cnt_cmd='sleep 1000'
               )
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 6
    assert count['not_running'] == 2
    assert count['running'] == 2
    assert count['paused'] == 2

    p = params(states=dict(absent=2), beacon='test_cnt',
               filters={'state': ['created_or_stopped']})
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 4
    assert count['running'] == 2
    assert count['paused'] == 2

    #to create new container is needed to provide same argument and options
    p = params(states=dict(started=1), beacon='test_cnt',
               filters={'state': ['paused']})
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 4
    assert count['running'] == 3
    assert count['paused'] == 1

    #start 2 created containers and stop others
    p = params(state='stopped', count='all', beacon='test_cnt',
               filters={'state': ['paused']},
               kill_on_stop=True)
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 4
    assert count['not_running'] == 1
    assert count['running'] == 3


    p = params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)
    count = cnt_count(docker)
    assert count['total'] == 0


def test_pocker_cnt_wait_for(image, cnt_params, docker):
    params = cnt_params

    #ensure clean state
    p = params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='started', beacon='test_cnt', count=2, cnt_img='test-img',
               cnt_cmd='sh -c "nc -l -p 8000 | nc -l -p 8001"',
               cnt_opts={'expose': [8000, 8001]},
               wait_for={'ports': [8000, 8001], 'timeout': 2})
    assert pocker_cnt(p)
    p = params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)
    assert len(docker.ps(all=True).result) == 0

    p = params(state='started', beacon='test_cnt', wait_for={'ports': 8000})
    try:
        pocker_cnt(p)
    except PCntFail as err:
        assert 'must be non-empty list' in err.message
    p = params(state='absent', beacon='test_cnt')
    assert pocker_cnt(p)

    p = params(state='started', beacon='test_cnt', count=2, cnt_img='test-img',
               cnt_cmd='sleep 2',
               cnt_opts={'expose': [9000, 9001]},
               wait_for={'ports': [8000]})
    try:
        pocker_cnt(p)
    except PCntFail as err:
        assert 'not present in container exposed ports' in err.message
    p = params(state='absent', beacon='test_cnt')
    assert pocker_cnt(p)

    p = params(state='started', beacon='test_cnt', count=2, cnt_img='test-img',
               cnt_cmd='sleep 2',
               cnt_opts={'expose': [8000]},
               wait_for={'ports': [8000], 'timeout': 1})
    try:
        pocker_cnt(p)
    except PCntFail as err:
        assert 'Exceeded wait_for_timeout' in err.message

    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)


def test_pocker_cnt_get_logs(image, cnt_params, docker):
    params = cnt_params

    #ensure clean state
    p = params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='started', beacon='test_cnt', count=2, cnt_img='test-img',
               cnt_cmd='echo pocker_log', get_logs=True)
    result = pocker_cnt(p)
    assert len(result.logs) == 2
    assert all(['pocker_log' in log for log in result.logs])

    p = params(state='started', beacon='test_cnt', get_logs=True)
    result = pocker_cnt(p)
    assert len(result.logs) == 2
    assert all(['pocker_log' in log for log in result.logs])

    p = params(state='started', beacon='test_cnt')
    result = pocker_cnt(p)
    assert len(result.logs) == 0
    assert not any(['pocker_log' in log for log in result.logs])

    #ensure clean state
    p = params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)


def test_pocker_cnt_pocker_env(image, cnt_params, docker):
    params = cnt_params

    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='started', beacon='test_cnt',
               cnt_img='test-img', cnt_cmd='true',
               cnt_opts={'env': dict(key='value')})
    try:
        pocker_cnt(p)
    except PCntFail as err:
        assert "'cnt_opts.env' must be list of strings" in err.message

    p = params(state='started', beacon='test_cnt',
               cnt_img='test-img', cnt_cmd='true',
               cnt_opts={'env': pocker_env(dict(key=1))})
    try:
        pocker_cnt(p)
    except PCntFail as err:
        assert "Non string value in 'cnt_opts.env'" in err.message

    p = params(state='started', beacon='test_cnt', count=1,
               cnt_img='test-img', cnt_cmd='true',
               cnt_opts={'env': pocker_env(dict(key=None, key2='string'))})
    assert pocker_cnt(p)

    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)


def test_pocker_cnt_beacon_and_name(image, cnt_params, docker):
    params = cnt_params

    #ensure clean state
    p = params(state='absent', beacon='test_cnt', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='started')
    try:
        pocker_cnt(p)
    except PCntFail as err:
        assert "'beacon' or 'cnt_opts.name' must be specified" in err.message

    p = params(state='started', cnt_opts={'name': 'name_as_beacon' }, count=2)
    try:
        pocker_cnt(p)
    except PCntFail as err:
        assert "count must be '1'" in err.message

    #use name as beacon if beacon not set and count==1
    p = params(state='started', cnt_img='test-img', count=1, cnt_cmd='sleep 2',
               cnt_opts={'name': 'name_as_beacon' })
    assert pocker_cnt(p)
    assert docker.inspect('name_as_beacon').result[0]['State']['Running'] is True
    assert docker.inspect('name_as_beacon').result[0]['Config']['Labels']['pocker._beacon'] == '"name_as_beacon"'
    p = params(state='force_reloaded', count=1, cnt_opts={'name': 'name_as_beacon'}, kill_on_stop=True)
    assert pocker_cnt(p).changed is True
    assert docker.inspect('name_as_beacon').result[0]['State']['Running'] is True
    p = params(state='force_reloaded', beacon='name_as_beacon', kill_on_stop=True)
    assert pocker_cnt(p).changed is True
    assert docker.inspect('name_as_beacon').result[0]['State']['Running'] is True

    p = params(state='absent', beacon='name_as_beacon', kill_on_stop=True)
    assert pocker_cnt(p)

    #use name as beacon if beacon and count not set
    p = params(state='started', cnt_img='test-img', cnt_cmd='sleep 2',
               cnt_opts={'name': 'name_as_beacon' })
    assert pocker_cnt(p)
    assert docker.inspect('name_as_beacon').result[0]['State']['Running'] is True

    p = params(state='absent', beacon='name_as_beacon', kill_on_stop=True)
    assert pocker_cnt(p)


def test_pocker_net(image, net_params, cnt_params, docker):
    params = net_params

    #ensure clean state
    p = params(state='absent', name='test_net')
    pocker_net(p)
    pocker_cnt(cnt_params(state='absent', cnt_img='test-img', count=1, cnt_opts={'name': 'cnt1'}))
    pocker_cnt(cnt_params(state='absent', cnt_img='test-img', count=1, cnt_opts={'name': 'cnt2'}))
    pocker_cnt(cnt_params(state='absent', cnt_img='test-img', count=1, cnt_opts={'name': 'cnt3'}))

    p = params(state='absent', name='test_net')
    assert pocker_net(p).changed is False

    p = params(state='present', name='test_net')
    assert pocker_net(p).changed is True

    p = cnt_params(state='started', cnt_img='test-img', count=1, cnt_cmd='sleep 5',
               cnt_opts={'name': 'cnt1', 'net': 'test_net' })
    cnt1_id = pocker_cnt(p).facts[0]['Id']
    p = cnt_params(state='started', cnt_img='test-img', count=1, cnt_cmd='sleep 5',
               cnt_opts={'name': 'cnt2' })
    cnt2_id = pocker_cnt(p).facts[0]['Id']
    p = cnt_params(state='started', cnt_img='test-img', count=1, cnt_cmd='sleep 5',
               cnt_opts={'name': 'cnt3' })
    cnt3_id = pocker_cnt(p).facts[0]['Id']

    conts_in_net = docker.network_inspect('test_net').result[0]['Containers']
    assert len(conts_in_net) == 1
    assert cnt1_id in conts_in_net.keys()

    p = params(state='present', name='test_net', connected=['cnt2', 'cnt3'],
               disconnected=['cnt1'])
    assert pocker_net(p).changed is True
    conts_in_net = docker.network_inspect('test_net').result[0]['Containers']
    assert len(conts_in_net) == 2
    assert cnt2_id in conts_in_net.keys()
    assert cnt3_id in conts_in_net.keys()
    assert not cnt1_id in conts_in_net.keys()

    p = params(state='present', name='test_net', connected=['cnt1'],
               disconnect_others=True)
    assert pocker_net(p).changed is True
    conts_in_net = docker.network_inspect('test_net').result[0]['Containers']
    assert len(conts_in_net) == 1
    assert cnt1_id in conts_in_net.keys()
    assert not cnt2_id in conts_in_net.keys()
    assert not cnt3_id in conts_in_net.keys()

    pocker_cnt(cnt_params(state='absent', cnt_img='test-img', count=1, cnt_opts={'name': 'cnt1'}))
    pocker_cnt(cnt_params(state='absent', cnt_img='test-img', count=1, cnt_opts={'name': 'cnt2'}))
    pocker_cnt(cnt_params(state='absent', cnt_img='test-img', count=1, cnt_opts={'name': 'cnt3'}))

    #test 'wait_for' with networks
    p = cnt_params(state='started', beacon='test_cnt', count=1, cnt_img='test-img',
               cnt_cmd='sh -c "nc -l -p 8000 | nc -l -p 8001"',
               cnt_opts={'expose': [8000, 8001], 'net': 'test_net'},
               wait_for={'ports': [8000, 8001], 'timeout': 2})
    assert pocker_cnt(p)

    p = cnt_params(state='absent', beacon='test_cnt', count='all', kill_on_stop=True)
    assert pocker_cnt(p)

    p = params(state='absent', name='test_net')
    assert pocker_net(p).changed is True

    #TODO: networks and beacons

    assert not 'test_net' in [cnt['Name'] for cnt in docker.network_ls().result]


def test_pocker_vol(image, vol_params, cnt_params, docker):
    params = vol_params

    #ensure clean state
    p = params(state='absent', vol_opts=dict(name='test_vol'))
    pocker_vol(p)
    p = params(state='absent', vol_opts=dict(name='test_vol2'))
    pocker_vol(p)

    p = params(state='absent', vol_opts=dict(name='test_vol'))
    assert pocker_vol(p).changed is False

    p = params(state='present', vol_opts=dict(name='test_vol'))
    assert pocker_vol(p).changed is True

    vol_ls = docker.volume_ls().result
    assert len(vol_ls) == 1
    assert vol_ls[0]['Name'] == 'test_vol'

    p = params(state='present', vol_opts=dict(name='test_vol2'))
    assert pocker_vol(p).changed is True

    vol_ins = docker.volume_inspect('test_vol', 'test_vol2').result
    assert len(vol_ins) == 2
    assert vol_ins[0]['Driver'] == 'local'

    p = params(state='absent', vol_opts=dict(name='test_vol'))
    assert pocker_vol(p).changed is True
    p = params(state='absent', vol_opts=dict(name='test_vol2'))
    assert pocker_vol(p).changed is True

    vol_ls = docker.volume_ls().result
    assert len(vol_ls) == 0



#TODO: tests for pocker_dmn












