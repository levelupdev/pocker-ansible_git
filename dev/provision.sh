#!/usr/bin/env bash
sudo apt-get update

echo "Update kernel for aufs..."
sudo apt-get -y install linux-image-generic-lts-trusty

echo "Installing host docker(to start docker-in-docker instances)..."
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get -y install docker-engine=1.11.3-0~trusty
sudo usermod -aG docker vagrant

echo "Force docker to use aufs..."
echo 'DOCKER_OPTS="-s aufs"' | sudo tee --append /etc/default/docker > /dev/null
sudo service docker restart

echo "Install additional pythons(2.6, 3.5) for tests ..."
sudo add-apt-repository -y ppa:fkrull/deadsnakes
sudo apt-get update
sudo apt-get -y install python python-dev
sudo apt-get -y install python2.6 python2.6-dev
sudo apt-get -y install python3.5 python3.5-dev

echo "Getting pip..."
wget https://bootstrap.pypa.io/get-pip.py -O get-pip.py
sudo python get-pip.py

echo "Installing development deps..."
sudo pip install -r ~/source/dev/requirements.txt
sudo chown -hR vagrant:vagrant ~/.config; #fix for pudb
cd ~/source
sudo python setup.py develop
